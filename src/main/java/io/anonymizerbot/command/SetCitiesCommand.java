package io.anonymizerbot.command;

import io.anonymizerbot.BotUtils;
import io.anonymizerbot.model.MezoUser;
import io.anonymizerbot.service.MezoUserService;
import io.anonymizerbot.service.WeatherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.IBotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
public class SetCitiesCommand implements IBotCommand {

    private static final Logger log = LogManager.getLogger(SetCitiesCommand.class);
    private static final String commandName = "set_city";
    private static final String description = "set your city for fast using /weather command \n";

    @Autowired
    MezoUserService mezoUserService;

    @Autowired
    WeatherService weatherService;

    @Override
    public String getCommandIdentifier() {
        return commandName;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Transactional
    @Override
    public void processMessage(AbsSender absSender, Message message, String[] arguments) {
        String city = weatherService.getCity(arguments);
        SendMessage outMessage = new SendMessage();
        outMessage.setChatId(message.getChatId());
        if (city == null) {
            outMessage.setText("Ooops, you forgot to enter the city");
        } else {
            MezoUser mezoUser = mezoUserService.findOne(message.getFrom().getId());
            if (mezoUser != null) {
                List<String> favoriteCities = mezoUser.getFavoriteCities();
                if (favoriteCities == null) {
                    favoriteCities = new ArrayList<>();
                }
                favoriteCities.clear();//TODO fix this
                favoriteCities.add(city);
                mezoUser.setFavoriteCities(favoriteCities);
                outMessage.setText("Ok, yor city is " + city);
            }
        }
        BotUtils.setButtons(outMessage);
        try {
            absSender.execute(outMessage);
        } catch (TelegramApiException e) {
            log.error("command - {}",  getCommandIdentifier(), e);
        }
    }
}
