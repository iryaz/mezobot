package io.anonymizerbot.command;

import io.anonymizerbot.AnnotationsImpl.Annotation.BotRegCommand;
import io.anonymizerbot.BotUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.IBotCommand;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.util.Set;

@Component

@BotRegCommand
public class HelpCommand extends MyCustomCommand {

    private static final Logger logger = LogManager.getLogger(HelpCommand.class);

    @Autowired
    private ICommandRegistry mCommandRegistry;

    @Autowired
    private Set<IBotCommand> allCommand;

    public HelpCommand() {
        super("help", "list all known commands\n");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

        logger.info("User- {}, command - {}", user.getId(), getCommandIdentifier());

        StringBuilder helpMessageBuilder = new StringBuilder("<b>Available commands:</b>\n");

        mCommandRegistry.getRegisteredCommands().forEach(cmd -> helpMessageBuilder.append(
                "<b>" + "----" + cmd.getCommandIdentifier() + "----" + "\n</b>"  +cmd.getDescription()).append("\n"));

        SendMessage helpMessage = new SendMessage();
        helpMessage.setChatId(chat.getId());
        helpMessage.enableHtml(true);
        helpMessage.setText(helpMessageBuilder.toString());
        BotUtils.setButtons(helpMessage);
        execute(absSender, helpMessage, user);
    }
}