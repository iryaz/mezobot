package io.anonymizerbot.command;

import io.anonymizerbot.AnnotationsImpl.Annotation.BotRegCommand;
import io.anonymizerbot.BotUtils;
import io.anonymizerbot.model.MezoUser;
import io.anonymizerbot.repository.MezoUserRepository;
import io.anonymizerbot.service.MezoUserService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.IBotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
@BotRegCommand
@Transactional
//@Scope(proxyMode = ScopedProxyMode.INTERFACES)
public class StartCommand implements IBotCommand {

    @Autowired
    private MezoUserRepository mezoUserRepository;

    @Autowired
    private MezoUserService mezoUserService;

    private static final Logger log = LogManager.getLogger(StartCommand.class);

    /**
     * реализованный метод класса BotRegCommand, в котором обрабатывается команда, введенная пользователем
     *
     * @param absSender - отправляет ответ пользователю
     * @param user      - пользователь, который выполнил команду
     * @param chat      - чат бота и пользователя
     * @param strings   - аргументы, переданные с командой
     */

    // @Override
    @Transactional
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        StringBuilder sb = new StringBuilder();
        SendMessage message = new SendMessage();
        message.setChatId(chat.getId().toString());

        MezoUser mezoUser = mezoUserRepository.findByTelegramId(user.getId());
        if (mezoUser == null) {
            mezoUserService.createtMezoUserByTelegramUser(user, chat);
            log.info("User {} is trying to execute '{}' the first time. Added to users' list.", user.getId(), getCommandIdentifier());
            sb.append("Shalom, ").append(user.getUserName() != null ? user.getUserName() : "dude").append("! You've been added to  users' list!\n")
                    .append("Please execute command:\n'/set_name <displayed_name>'\nwhere <displayed_name>; is the name you want to use to hide your real name.");
        } else {
            sb.append(mezoUser.getUsername()).append(", you've already started!");
        }
        message.setText(sb.toString());
        BotUtils.setButtons(message);
        try {
            absSender.execute(message);
            log.log(Level.getLevel(Level.INFO.name()), "", user.getId(), getCommandIdentifier());
        } catch (TelegramApiException e) {
            log.error("User- {}, command - {}", user.getId(), getCommandIdentifier(), e);
        }

    }

    @Override
    public String getCommandIdentifier() {
        return "start";
    }

    @Override
    public String getDescription() {
        return "start using\n";
    }

    @Override
    @Transactional
    public void processMessage(AbsSender absSender, Message message, String[] arguments) {
        execute(absSender, message.getFrom(), message.getChat(), arguments);
    }
}