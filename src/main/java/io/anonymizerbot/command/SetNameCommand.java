package io.anonymizerbot.command;

import io.anonymizerbot.AnnotationsImpl.Annotation.BotRegCommand;
import io.anonymizerbot.BotUtils;
import io.anonymizerbot.service.MezoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Component
@BotRegCommand
public class SetNameCommand extends MyCustomCommand {

    @Autowired
    private MezoUserService mezoUserService;

    public SetNameCommand() {
        super("set_name", "set or change name that will be displayed with your messages\n");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

        String displayedName = getName(strings);
        displayedName = displayedName != null ? displayedName : user.getUserName();

        SendMessage message = new SendMessage();
        message.setChatId(chat.getId().toString());

        if (displayedName == null) {
            message.setText("You should use non-empty name!");
            execute(absSender, message, user);
            return;
        }

        if (!mezoUserService.setUserName(user.getId(), displayedName)) {
            message.setText("Firstly you should start! Execute '/start' command!");
            execute(absSender, message, user);
            return;
        }

        message.setText("Your displayed name: " + displayedName);
        BotUtils.setButtons(message);
        execute(absSender, message, user);
    }

    private String getName(String[] strings) {

        if (strings == null || strings.length == 0) {
            return null;
        }

        String name = String.join(" ", strings);
        return name.replaceAll(" ", "").isEmpty() ? null : name;
    }
}