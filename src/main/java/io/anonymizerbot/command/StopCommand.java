package io.anonymizerbot.command;

import io.anonymizerbot.AnnotationsImpl.Annotation.BotRegCommand;
import io.anonymizerbot.service.MezoUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Component
@BotRegCommand
public final class StopCommand extends MyCustomCommand {

    private static final Logger logger = LogManager.getLogger(StopCommand.class);

    @Autowired
    MezoUserService mezoUserService;

    public StopCommand() {
        super("stop", "remove yourself from users' list\n");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        SendMessage message = new SendMessage();
        message.setChatId(chat.getId().toString());
        message.setText("Buy, dude! See you later");
        logger.info("MezoUser by Id-{} was deleted", user.getId());
        mezoUserService.deleteUser(user.getId());
        execute(absSender, message, user);
    }
}