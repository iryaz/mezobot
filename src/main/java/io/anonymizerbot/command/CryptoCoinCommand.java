package io.anonymizerbot.command;


import io.anonymizerbot.AnnotationsImpl.Annotation.BotRegCommand;
import io.anonymizerbot.BotUtils;
import io.anonymizerbot.model.MezoUser;
import io.anonymizerbot.service.CriptoService;
import io.anonymizerbot.service.MezoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.text.ParseException;
import java.util.List;


@Component
@BotRegCommand
public class CryptoCoinCommand extends MyCustomCommand {

    @Autowired
    CriptoService criptoService;

    @Autowired
    private MezoUserService mezoUserService;

    public CryptoCoinCommand() {
        super("crypto", "Crypto currency rate at selected rate\n");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

        log.info("", user.getId(), getCommandIdentifier());
        try {
            SendMessage outMessage = new SendMessage();
            String rate = getRateName(strings);
            if (rate == null || rate.isEmpty()) {
                MezoUser mezoUser = mezoUserService.findOne(user.getId());
                if (mezoUser != null) {
                    List<String> userRates = mezoUserService.getUserRates(user.getId());
                    if (userRates != null || !userRates.isEmpty()) {
                        rate = userRates.get(0);
                    }
                }
                else {
                    log.error("Not found user by id = {}", user.getId());
                    return;
                }
            }
            String info = criptoService.getRateInfo(rate);
            outMessage.setChatId(chat.getId().toString());
            outMessage.setText(info);
            BotUtils.setButtons(outMessage);
            execute(absSender, outMessage, user);
        } catch (ParseException ex) {
            log.error(ex.getStackTrace());
        }

    }

    private String getRateName(String[] strings) {
        if (strings == null || strings.length == 0) {
            return null;
        }
        String name = String.join(" ", strings);
        return name.replaceAll(" ", "").isEmpty() ? null : name;
    }
}