package io.anonymizerbot.command;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

abstract class MyCustomCommand extends BotCommand {

    final Logger log = LogManager.getLogger(getClass());

    MyCustomCommand(String commandIdentifier, String description) {
        super(commandIdentifier, description);
    }

    protected void execute(AbsSender sender, SendMessage message, User user) {
        try {
            sender.execute(message);
            log.log(Level.getLevel(Level.INFO.name()), "", user.getId(), getCommandIdentifier());
        } catch (TelegramApiException e) {
            log.error( "User- {}, command - {}", user.getId(), getCommandIdentifier(), e);
        }
    }
}