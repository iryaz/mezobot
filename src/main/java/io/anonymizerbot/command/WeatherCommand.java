package io.anonymizerbot.command;

import io.anonymizerbot.AnnotationsImpl.Annotation.BotRegCommand;
import io.anonymizerbot.BotUtils;
import io.anonymizerbot.model.Weather;
import io.anonymizerbot.service.MezoUserService;
import io.anonymizerbot.service.WeatherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.io.IOException;
import java.util.List;


@Component
@BotRegCommand
public class WeatherCommand extends MyCustomCommand {

    private static final Logger logger = LogManager.getLogger(WeatherCommand.class);

    @Autowired
    WeatherService weatherService;

    @Autowired
    MezoUserService mezoUserService;

    public WeatherCommand() {
        super("weather", "weather in selected city\n");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        log.info("{} - {}", user.getId(), getCommandIdentifier());
        try {
            SendMessage weatherMessage = new SendMessage();
            String city = weatherService.getCity(strings);
            if (city == null) {
                List<String> userCities = mezoUserService.getUserCities(user.getId());
                if (userCities != null || !userCities.isEmpty()) {
                    city = userCities.get(0);
                }
            }
            if (city == null) {
                weatherMessage.setText("Buba, i don't feel legs");
            } else {
                Weather weather = weatherService.getWeather(city);
                weatherMessage.setChatId(chat.getId().toString());
                weatherMessage.enableHtml(true);
                weatherMessage.setText(weather != null ? weather.getUserInfo(weatherService.getEmoji(weather.getMain())) : "unknown city");
            }
            BotUtils.setButtons(weatherMessage);
            execute(absSender, weatherMessage, user);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }


}
