package io.anonymizerbot.command;

import io.anonymizerbot.BotUtils;
import io.anonymizerbot.model.MezoUser;
import io.anonymizerbot.service.MezoUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.IBotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;


@Component
public class SetCryptoRate  implements IBotCommand {

    private static final Logger log = LogManager.getLogger(SetCryptoRate.class);
    private static final String commandName = "set_crypto";
    private static final String description = "set your favorite rate for fast using /crypto command \n";

    @Autowired
    MezoUserService mezoUserService;

    @Override
    public String getCommandIdentifier() {
        return commandName;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Transactional
    @Override
    public void processMessage(AbsSender absSender, Message message, String[] arguments) {
        String rate = BotUtils.parseArgs(arguments);
        SendMessage outMessage = new SendMessage();
        outMessage.setChatId(message.getChatId());
        if (rate == null) {
            outMessage.setText("Ooops, you forgot to enter the rate(Bitcoin, Ripple ...)");
        } else {
            MezoUser mezoUser = mezoUserService.findOne(message.getFrom().getId());
            if (mezoUser != null) {
                List<String> favoriteRates = mezoUser.getFavoriteRates();
                if (favoriteRates == null) {
                    favoriteRates = new ArrayList<>();
                }
                favoriteRates.clear();//TODO fix this
                favoriteRates.add(rate);
                mezoUser.setFavoriteRates(favoriteRates);
                outMessage.setText("Ok, yor rate is " + rate);
            }
        }
        BotUtils.setButtons(outMessage);
        try {
            absSender.execute(outMessage);
        } catch (TelegramApiException e) {
            log.error("command - {}",  getCommandIdentifier(), e);
        }
    }


}
