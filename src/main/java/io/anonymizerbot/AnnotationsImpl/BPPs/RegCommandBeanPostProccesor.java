package io.anonymizerbot.AnnotationsImpl.BPPs;

import io.anonymizerbot.AnnotationsImpl.Annotation.BotRegCommand;
import io.anonymizerbot.AnnotationsImpl.Annotation.CommandRegistrator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.IBotCommand;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

//@Component //кривая фигня, не все бины настроены
public class RegCommandBeanPostProccesor implements BeanPostProcessor {

    Map<String, Object> commands = new HashMap<>();

    private static final Logger logger = LogManager.getLogger(RegCommandBeanPostProccesor.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
         Annotation annotation = bean.getClass().getAnnotation(BotRegCommand.class);
        if (annotation != null) {
            commands.put(beanName, bean);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Annotation annotation = bean.getClass().getAnnotation(CommandRegistrator.class);
        if (annotation != null) {
            try {
                //bean.getClass().get
                Method register = bean.getClass().getSuperclass().getDeclaredMethod("register", IBotCommand.class);//TODO: FIXME RECURSIVE CHECK
                //Method setButtons = bean.getClass().getSuperclass().getDeclaredMethod("setButtons", IBotCommand.class);
                if (register != null){
                    for (Map.Entry<String, Object> entry : commands.entrySet()){
                       register.invoke(bean, entry.getValue());
                       logger.info("Register command - {}", entry.getKey());
                    }
                }
            } catch (Exception e) {
                logger.info(e.getMessage());
            }
        }
        return bean;
    }
}
