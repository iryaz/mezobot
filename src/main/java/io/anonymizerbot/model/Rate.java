package io.anonymizerbot.model;

import java.util.Date;

public class Rate {
    private String name;
    private Integer Id;
    private Double price;
    private Double volume24h;
    private Date lastUpdate;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getVolume24h() {
        return volume24h;
    }

    public void setVolume24h(Double volume24h) {
        this.volume24h = volume24h;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        String result = "\uD83D\uDCB8 " + getName() + "\n"
                + "\uD83D\uDCB5 " + String.format("%.2f", getPrice()) + " USD" + "\n"
                + "⌚ volume24h:  " + getVolume24h() + "\n"
                + "last Update:  " + getLastUpdate();
        return result;
    }
}
