package io.anonymizerbot.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {

    USER,
    PRIVELEGE_USER;

    @Override
    public String getAuthority() {
        return name();
    }
}
