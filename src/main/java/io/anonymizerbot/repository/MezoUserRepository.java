package io.anonymizerbot.repository;

import io.anonymizerbot.model.MezoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MezoUserRepository extends JpaRepository<MezoUser, Long> {
    MezoUser findByUsername(String username);

    List<MezoUser> findBychatId(long chatId);

    MezoUser findByTelegramId(long telegramId);

    void deleteByTelegramId(long telegtamId);
}
