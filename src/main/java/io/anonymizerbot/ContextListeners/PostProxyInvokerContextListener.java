package io.anonymizerbot.ContextListeners;

import io.anonymizerbot.AnnotationsImpl.Annotation.BotRegCommand;
import io.anonymizerbot.AnnotationsImpl.Annotation.PostProxyConstruct;
import io.anonymizerbot.command.StopCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
public class PostProxyInvokerContextListener implements ApplicationListener <ContextRefreshedEvent> {

    @Autowired
    private ConfigurableListableBeanFactory factory;

    private static final Logger logger = LogManager.getLogger(PostProxyInvokerContextListener.class);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        ApplicationContext applicationContext =  event.getApplicationContext();
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();

        for (String beanDefinitionName : beanDefinitionNames) {
            BeanDefinition beanDefinition = factory.getBeanDefinition(beanDefinitionName);
            String OriginalBeanClassName = beanDefinition.getBeanClassName();
            try {
                Class<?> orignalClass = Class.forName(OriginalBeanClassName);
                Method[] methods = orignalClass.getMethods();
                for (Method method : methods) {
                    if (method.isAnnotationPresent(PostProxyConstruct.class)) {
                        Object bean = applicationContext.getBean(beanDefinitionName);
                        Method registerCommands = bean.getClass().getMethod(method.getName(), method.getParameterTypes());
                        registerCommands.invoke(bean);
                    }
                }
                if (orignalClass.isAnnotationPresent(BotRegCommand.class)){
                    break;
                }

            } catch (Exception ex) {
               logger.error(ex.getStackTrace());
            }
        }
    }
}
