package io.anonymizerbot.service;

import io.anonymizerbot.model.MezoUser;
import io.anonymizerbot.repository.MezoUserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.List;

@Service
public class MezoUserService {

    private static final Logger logger = LogManager.getLogger(MezoUserService.class);

    @Autowired
    MezoUserRepository mezoUserRepository;

    public MezoUser findOne(long telegramId) {
        MezoUser mezoUser = mezoUserRepository.findByTelegramId(telegramId);
        if (mezoUser == null) {
            logger.error("User was tom found by telegramId - {} ", telegramId);
        }
        return mezoUser;
    }

    @Transactional
    public void createtMezoUserByTelegramUser(User user, Chat chat) {
        MezoUser mezoUser = new MezoUser();
        mezoUser.setUsername(user.getFirstName());
        mezoUser.setChatId(chat.getId());
        mezoUser.setTelegramId(user.getId());
        mezoUserRepository.saveAndFlush(mezoUser);
    }

    public String getUserName(long telegramId) {
        MezoUser mezoUser = mezoUserRepository.findByTelegramId(telegramId);
        return mezoUser != null ? mezoUser.getUsername() : "dude";
    }

    public List<String> getUserCities (long telegramId){
        MezoUser mezoUser = mezoUserRepository.findByTelegramId(telegramId);
        if (mezoUser != null) {
            return mezoUser.getFavoriteCities();
        } else {
            return null;
        }
    }

    public List<String> getUserRates (long telegramId){
        MezoUser mezoUser = mezoUserRepository.findByTelegramId(telegramId);
        if (mezoUser != null) {
            return mezoUser.getFavoriteRates();
        } else {
            return null;
        }
    }

    public MezoUser findByUsername(String name) {
        return mezoUserRepository.findByUsername(name);
    }

    @Transactional
    public void deleteUser(long telegtramId) {
        mezoUserRepository.deleteByTelegramId(telegtramId);

    }

    @Transactional
    public boolean setUserName(long telegramId, String username) {
        MezoUser mezoUser = mezoUserRepository.findByTelegramId(telegramId);
        if (mezoUser != null) {
            mezoUser.setUsername(username);
            logger.info("MezoUser by Id-{} change name, {} -> {}", telegramId, mezoUser.getUsername(), username);
            return true;
        } else {
            logger.info("MezoUser by Id-{} not found ", telegramId);
            return false;
        }
    }
}
