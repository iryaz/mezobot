package io.anonymizerbot.service;

import io.anonymizerbot.model.Rate;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


@Service
public class CriptoService {

    private static final String apiKey = "2ae71a5f-b3fd-4eff-88d2-bb645ade589b";
    private static final DateFormat iso8601TimestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public String getRates() {
        String uri = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest";
        List<NameValuePair> paratmers = new ArrayList<NameValuePair>();
        paratmers.add(new BasicNameValuePair("start", "1"));
        paratmers.add(new BasicNameValuePair("limit", "5000"));
        paratmers.add(new BasicNameValuePair("convert", "USD"));

        try {
            String result = makeAPICall(uri, paratmers);
            //  System.out.println(result);
            return result;
        } catch (IOException e) {
            System.out.println("Error: cannont access content - " + e.toString());
        } catch (URISyntaxException e) {
            System.out.println("Error: Invalid URL " + e.toString());
        }
        return "";
    }

    public String makeAPICall(String uri, List<NameValuePair> parameters) //TODO заменить на resttemplate
            throws URISyntaxException, IOException {
        String response_content = "";

        URIBuilder query = new URIBuilder(uri);
        query.addParameters(parameters);

        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet request = new HttpGet(query.build());
        request.setHeader(HttpHeaders.ACCEPT, "application/json");
        request.addHeader("X-CMC_PRO_API_KEY", apiKey);

        CloseableHttpResponse response = client.execute(request);

        try {
            System.out.println(response.getStatusLine());
            HttpEntity entity = response.getEntity();
            response_content = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }

        return response_content;
    }

    public String getRateInfo(String rate) throws ParseException {
        String allRates = getRates();
        JSONObject object = new JSONObject(allRates);
        JSONArray data = object.getJSONArray("data");
        Rate resultRate = new Rate();
        for (int i = 0; i < data.length(); i++) {
            JSONObject dataRate = data.getJSONObject(i);
            String findName = dataRate.getString("name");
            if (!rate.equals(findName)) {
                continue;
            }
            resultRate.setName(findName);
            JSONObject reateInfo = dataRate.getJSONObject("quote").getJSONObject("USD");
            resultRate.setPrice(reateInfo.getDouble("price"));
            resultRate.setVolume24h(reateInfo.getDouble("volume_24h"));
            String dateStr = reateInfo.getString("last_updated");
            resultRate.setLastUpdate(iso8601TimestampFormat.parse(dateStr));
            break;
        }
        return resultRate.toString();
    }

}