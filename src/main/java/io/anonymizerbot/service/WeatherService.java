package io.anonymizerbot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.anonymizerbot.BotUtils;
import io.anonymizerbot.model.Weather;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class WeatherService {

    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();

    private static Map<String, String> mapEmoji = new ConcurrentHashMap<>();

    private static final String token = "3feae23be73d960a58019e9b3035d403";

    private static final String siteUrl = "http://api.openweathermap.org/data/2.5/weather?q=";

    @Autowired
    private RestTemplate restTemplate;

    @PostConstruct
    private void init(){
        mapEmoji.put("Rain", "☔");
        mapEmoji.put("Snow", "❄");
        mapEmoji.put("Mist", "\uD83D\uDEAC");
    }

    public String getEmoji(String main) {
        return mapEmoji.get(main);
    }

    public Weather getWeather(String city) throws IOException {
        String json = restTemplate.getForObject(siteUrl +  city + "&units=metric&appid="  + token, String.class);
        JSONObject object = new JSONObject(json);

        Weather weather = new Weather();
        weather.setName(object.getString("name"));

        JSONObject main = object.getJSONObject("main");

        weather.setTemperature(main.getDouble("temp"));
        weather.setHumidity(main.getDouble("humidity"));

        JSONArray jsonArray = object.getJSONArray("weather");
        for (int i =0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            weather.setIcon(jsonObject.getString("icon"));
            weather.setMain(jsonObject.getString("main"));
        }
        return weather;
    }

    public String getCity(String[] strings) {
        return BotUtils.parseArgs(strings);
    }
}
