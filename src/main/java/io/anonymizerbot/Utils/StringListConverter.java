package io.anonymizerbot.Utils;

import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Converter
public class StringListConverter implements AttributeConverter<List<String>, String> {

    @Override
    public String convertToDatabaseColumn(List<String> list) {
        if(list != null && !list.isEmpty()) {
            return String.join(",", list);
        }
        return null;
    }

    @Override
    public List<String> convertToEntityAttribute(String joined) {
        if(!StringUtils.hasText(joined)){
            return null; //  maybe Collections.EMPTY_LIST !!!!!!
        }
        return new ArrayList<>(Arrays.asList(joined.split(",")));
    }

}
