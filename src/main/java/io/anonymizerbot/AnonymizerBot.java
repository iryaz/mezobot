package io.anonymizerbot;

import io.anonymizerbot.AnnotationsImpl.Annotation.CommandRegistrator;
import io.anonymizerbot.AnnotationsImpl.Annotation.PostProxyConstruct;
import io.anonymizerbot.command.CryptoCoinCommand;
import io.anonymizerbot.service.MezoUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.IBotCommand;
import org.telegram.telegrambots.meta.ApiContext;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import java.util.Set;


@Component
@CommandRegistrator
public final class AnonymizerBot extends TelegramLongPollingCommandBot {

    private static final Logger logger = LogManager.getLogger(AnonymizerBot.class);
    private static final String BOT_NAME = "MEZAbot";
    private static final String BOT_TOKEN = "1027286381:AAGHrmt7ijZxHlRrs6exRCFLEar909yC5Iw";
    private static final String UNKNOWN_COMMAND = "I'm sorry %s, I not understand you, check the list of commands.";

    @Autowired
    private MezoUserService mezoUserService;

    @Autowired
    private Set<IBotCommand> registerSet;

    @Autowired
    CryptoCoinCommand criptoCoinCommand;

    public AnonymizerBot() {
        super(ApiContext.getInstance(DefaultBotOptions.class), false);
        logger.info("Initializing Anonymizer Bot...");
    }

    @PostConstruct
    private void init() {

    }

    @PostProxyConstruct
    public void registerCustomCommand() {
        registerSet.stream().forEach(command -> register(command));
    }

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }


    // обработка сообщения не начинающегося с '/'
    @Override
    public void processNonCommandUpdate(Update update) {

        logger.info("Processing non-command update...");

        if (!update.hasMessage()) {
            logger.error("Update doesn't have a body!");
            throw new IllegalStateException("Update doesn't have a body!");
        }

        Message userMessage = update.getMessage();
        User user = userMessage.getFrom();
        SendMessage answer = new SendMessage();

        answer.setText(String.format(UNKNOWN_COMMAND, mezoUserService.getUserName(user.getId())));
        answer.setReplyToMessageId(userMessage.getMessageId());
        answer.setChatId(userMessage.getChatId());
        BotUtils.setButtons(answer);
        replyToUser(answer, user);

    }

    private void sendMessageToUser(SendMessage message, User receiver, User sender) {
        try {
            BotUtils.setButtons(message);
            execute(message);
            // logger.log(Level.getLevel(LogLevel.SUCCESS.getValue()), LogTemplate.MESSAGE_RECEIVED.getTemplate(), receiver.getId(), sender.getId());
        } catch (TelegramApiException e) {
            //logger.error(LogTemplate.MESSAGE_LOST.getTemplate(), receiver.getId(), sender.getId(), e);
        }
    }

    private void replyToUser(SendMessage message, User user, String messageText) {
        message.setText(messageText);
        replyToUser(message, user);
    }

    private void replyToUser(SendMessage message, User user) {
        try {
            execute(message);
            //logger.log(Level.getLevel(LogLevel.SUCCESS.getValue()), LogTemplate.MESSAGE_SENT.getTemplate(), user.getId(), messageText);
        } catch (TelegramApiException e) {
            //logger.error(LogTemplate.MESSAGE_EXCEPTION.getTemplate(), user.getId(), e);
        }
    }

}